package ch.strey.checkout.impl;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.junit.Test;

public class EasyRuleCheckoutTest {
	@Test
	public void testIncremental() {
		EasyRuleCheckout checkout = new EasyRuleCheckout(new BufferedReader(
				new InputStreamReader(getClass().getClassLoader().getResourceAsStream("testRules.yml"))));

		assertEquals("the total price is", 0, checkout.total());
		checkout.scan(new Product("A"));
		assertEquals("the total price is", 50, checkout.total());
		checkout.scan(new Product("B"));
		assertEquals("the total price is", 80, checkout.total());
		checkout.scan(new Product("A"));
		assertEquals("the total price is", 130, checkout.total());
		checkout.scan(new Product("A"));
		assertEquals("the total price is", 160, checkout.total());
		checkout.scan(new Product("B"));
		assertEquals("the total price is", 175, checkout.total());
	}
}

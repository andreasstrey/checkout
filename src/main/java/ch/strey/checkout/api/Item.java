package ch.strey.checkout.api;

/**
 * Your domain objects for products have to implement this interface so that they can be handled by {@link ch.strey.checkout.api.Checkout}.
 * 
 * @author andreasstrey
 *
 */
public interface Item {

	/**
	 * The Stock Keeping Unit for this item.
	 * 
	 * @return the Stock Keeping Units
	 */
	String getSKU();
}

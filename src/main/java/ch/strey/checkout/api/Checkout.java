package ch.strey.checkout.api;

/**
 * A system to handle pricing schemes and calculate the total price for a checkout in a supermarket.
 * 
 * @author andreasstrey
 *
 */
public interface Checkout {

	/**
	 * scan one new item.
	 * 
	 * @param item the item with a unique SKU.
	 */
	void scan(Item item);

	/**
	 * returns the total price of all items in the checkout
	 * 
	 * @return the total price
	 */
	long total();
}

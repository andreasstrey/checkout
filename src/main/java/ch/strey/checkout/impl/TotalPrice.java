package ch.strey.checkout.impl;

public class TotalPrice {
	private long price;

	public TotalPrice() {
		this.price = 0;
	}

	public void add(long value) {
		price += value;
	}

	public void multiply(double value) {
		this.price *= value;
	}

	public long getPrice() {
		return price;
	}

}

package ch.strey.checkout.impl;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.mvel.MVELRuleFactory;

import ch.strey.checkout.api.Checkout;
import ch.strey.checkout.api.Item;

/**
 * This implementation is based on the rule engine of j-easy/easy-rules. The reasons to use this framework are:
 * 
 * 1. the rules can be changed on runtime.
 * 2. we are able to define the rules based on a yaml file
 * 3. yaml is format which is based on a plain text file and can be edited on every operating system
 * 4. the expression language MVEL is easy to learn and verify similar to java.
 * 
 * You will find more information under @see <a href="https://github.com/j-easy/easy-rules">j-easy/easy-rules</a>
 * 
 * This class is NOT threadsafe.
 * 
 * @author andreasstrey
 *
 */
public class EasyRuleCheckout implements Checkout {

	private final Rules rules;

	private final RulesEngine engine;

	private final Map<String, ItemQuantity> scanned;

	public EasyRuleCheckout(Reader rulesSource) {
		this.rules = MVELRuleFactory.createRulesFrom(rulesSource);
		this.scanned = new HashMap<>();
		this.engine = new DefaultRulesEngine();
	}

	@Override
	public void scan(Item item) {
		if (item != null) {
			scanned.computeIfAbsent(item.getSKU(), key -> new ItemQuantity()).increment();
		}
	}

	@Override
	public long total() {
		Facts facts = new Facts();

		/*
		 * a small hack because easy-rules does not support direct results. Sideeffects
		 * are not nice but it is the recommended way.
		 */
		facts.put("total", new TotalPrice());

		for (Map.Entry<String, ItemQuantity> entry : scanned.entrySet()) {
			// The ruleengine could change the quantity. Thats why we need a copy.
			facts.put(entry.getKey(), new ItemQuantity(entry.getValue().getQuantity()));
		}

		engine.fire(rules, facts);
		TotalPrice totalPrice = facts.get("total");
		return totalPrice.getPrice();
	}
}

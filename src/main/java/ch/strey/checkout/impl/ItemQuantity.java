package ch.strey.checkout.impl;

public class ItemQuantity {
	public int quantity;

	public ItemQuantity() {
		this.quantity = 0;
	}

	public ItemQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void increment() {
		this.quantity += 1;
	}

	public void add(int value) {
		this.quantity += value;
	}

	public void subtract(int value) {
		this.quantity -= value;
	}

	public int getQuantity() {
		return this.quantity;
	}
}

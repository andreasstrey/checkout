package ch.strey.checkout.impl;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class EasyRuleCheckoutParameterizedTest {

	@Parameters(name = "{index}: {0}")
	public static Collection<Parameter> data() {
		return Arrays.asList( //
				new Parameter(0), //
				new Parameter(50, new String[] { "A" }), //
				new Parameter(80, new String[] { "A", "B" }), //
				new Parameter(115, new String[] { "C", "D", "B", "A" }), //
				new Parameter(100, new String[] { "A", "A" }), //
				new Parameter(130, new String[] { "A", "A", "A" }), //
				new Parameter(230, new String[] { "A", "A", "A", "A", "A" }), //
				new Parameter(260, new String[] { "A", "A", "A", "A", "A", "A" }), //
				new Parameter(160, new String[] { "A", "A", "A", "B" }), //
				new Parameter(175, new String[] { "A", "A", "A", "B", "B" }), //
				new Parameter(190, new String[] { "A", "A", "A", "B", "B", "D" }), //
				new Parameter(190, new String[] { "D", "A", "B", "A", "B", "A" }), //
				new Parameter(90, new String[] { "C", "C", "C", "C", "C" }));
	}

	private String[] items;

	private int expected;

	public EasyRuleCheckoutParameterizedTest(Parameter parameter) {
		this.expected = parameter.expected;
		this.items = parameter.items;

	}

	@Test
	public void checkTotalPrice() {
		EasyRuleCheckout checkout = new EasyRuleCheckout(new BufferedReader(
				new InputStreamReader(getClass().getClassLoader().getResourceAsStream("testRules.yml"))));
		for (String item : items) {
			checkout.scan(new Product(item));
		}
		assertEquals("the total price is", expected, checkout.total());
	}

	private static class Parameter {
		final int expected;
		final String[] items;

		public Parameter(int expected, String... items) {
			this.expected = expected;
			this.items = items;
		}

		@Override
		public String toString() {
			return Arrays.toString(items) + " should cost " + expected + "cent";
		}
	}
}

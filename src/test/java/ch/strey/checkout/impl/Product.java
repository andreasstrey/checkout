package ch.strey.checkout.impl;

import ch.strey.checkout.api.Item;

public class Product implements Item {

	private final String id;

	public Product(String id) {
		this.id = id;
	}

	public String getSKU() {
		return id;
	}
}
